class CreateMyproducts < ActiveRecord::Migration[5.1]
  def change
    create_table :myproducts do |t|
      t.string :name
      t.integer :price
      t.references :user, foreign_key: true

      t.timestamps
    end
        add_index :myproducts, [:user_id, :created_at]
  end
end
