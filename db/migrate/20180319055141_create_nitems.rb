class CreateNitems < ActiveRecord::Migration[5.1]
  def change
    create_table :nitems do |t|
      t.text :name
      t.text :price
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
