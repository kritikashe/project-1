class User < ApplicationRecord
  rolify
    #has_many :mypoducts, dependent: :destroy
    has_many :ipurchases, dependent: :destroy
    has_many :buitems, dependent: :destroy
    has_many :nitems, dependent: :destroy


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
