class Nitem < ApplicationRecord
  belongs_to :user
   validates :price,  presence: true, :numericality => { :greater_than_or_equal_to => 0 }, length: { maximum: 20}
end
