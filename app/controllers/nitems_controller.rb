class NitemsController < ApplicationController
	def index
  	end
	def create
      @xx = current_user.nitems.build(micropost_params)
      @xx.save
      
      if @xx.save
      redirect_to home_path
      added_product= Myitem.new
      added_product.name = @xx.name
      added_product.price = @xx.price
      added_product.save    
      else
         render 'shared/_error_messages' 
      end
  end
	def home
		@news=Nitem.all

	    render 'nitems/home'
	end

    def micropost_params
      params.require(:nitem).permit(:name ,:price)
    end

    def add_item
    	@what = Nitem.all
        render 'nitems/add_item'

    end
	
end
