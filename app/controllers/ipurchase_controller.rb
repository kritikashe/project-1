class IpurchaseController < ApplicationController

	def index

#@cart=current_user.ipurchases 
     @cart = Ipurchase.all 
#page(params[:page]).per(60)
     #puts @cart.inspect
    end

    def create
	  #puts params[:id].inspect
    
    #flash[:success] = "All good!"
	  id = params[:id]
	  @addcartitem = Myitem.find(id)
      cart_product = Ipurchase.new
      cart_product.name = @addcartitem.name
      cart_product.price = @addcartitem.price
      cart_product.user_id = current_user.id
      cart_product.save
      flash[:success] = "Added to Cart Items"
      redirect_to ipurchase_index_path 
    end

    def destroy
       id = params[:id]
      remove_cart = Ipurchase.find(id)
      remove_cart.destroy
      redirect_to ipurchase_index_path
    end
end
