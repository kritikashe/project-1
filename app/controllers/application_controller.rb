class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  #rescue_from CanCan::AccessDenied do |exception|
        #flash[:error] = exception.message
 
 protected

 def after_sign_in_path_for(resource)
    if resource.has_role? :admin
      root_url
    else
      alls_path
    end
  end
end
