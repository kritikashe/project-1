Rails.application.routes.draw do
  devise_for :users
  get 'users/new'

  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  # get  '/signup',  to:   new_user_registration_path 
  resources :users
  resources :alls
  resources :ipurchase
  resources :buitem
  resources :nitems
 get  '/home',    to: 'nitems#home'
 get  '/add_item',    to: 'nitems#add_item'

#  get "/add_item", to: "newitem#new"
end
